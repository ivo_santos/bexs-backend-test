# Aplicação #

Essa é uma aplicação feita em node.js para o teste de backend. 

### Foco em código claro ###

Criei a aplicação com foco em deixar o mais fácil possível de realizar manutenções, visto que esse é um problema que temos em grandes aplicações.

### Padrão de pastas ###
Toda regra de negócio está isolada na pasta services, onde existem arquivos com responsabilidade única.

### Validações e algoritmo dijkstra ###
Apliquei algumas simples validações ao receber uma chamada para adicionar uma nova rota.

Para conseguir resolver o problema de menor preço para a rota, utilizei o algoritmo de dijkstra.

Detalhe sobre o funcionamento pode ser visto em https://www.youtube.com/watch?v=ovkITlgyJ2s

## Como utilizar? ##

Necessário ter uma versão do node de preferência LTS mais recente.

##### Instale as dependências #####

	npm i

#### Acesso a interface de CONSOLE: ####
	npm run cli

Atráves dessa interface você poderá consultar uma rota com preço mais baixo. 
Utilize o formato FROM-TO.    

A resposta no console será a seguinte best route: GRU - BRC > $10

Em caso de rota não encontrada Route or destiny is unavailable.


#### Acesso a interface REST: ####
	npm run api

Para buscar informação de preço mais baixo para uma rota, é necessário consultar atráves da url abaixo.
A resposta desse chamada retornará os atributos routes que é um array do caminho e o price que é o valor.
Alterando os path parameters. {from} e {to}. 

[GET] -> http://localhost:3000/v1/destinies/{from}/{to}/best-price

ex: -> http://localhost:3000/v1/destinies/GRU/BRC/best-price

O endpoint abaixo permite adicionar uma nova rota. O corpo desse request exige origem, destino e preço.
[POST] -> http://localhost:3000/v1/destinies

ex: http://localhost:3000/v1/destinies
Body: 
{
    "from": "GRU",
    "to": "ORC",
    "price": "22"
}


#### Para rodar os testes unitários ####
	npm run test



