const inquirer = require('inquirer');
const bestPrice = require('../services/bestPrice');

const cli = () => {
    return inquirer.prompt([
        {
            name: 'routeQuestion',
            message: 'please enter the route: ex(FROM-TO)',
        }
    ]).then(answers => {
        return answers.routeQuestion;
    });
}

const getBestPrice = async () => {

    try {

        try {

            let answer = await cli();
            answer = answer.toUpperCase().split('-');

            const from = answer[0];
            const to = answer[1];

            const data = await bestPrice.get(from, to);
            console.log(`best route: ${data.route.join(' - ')} > $${data.total}`);
        } catch (error) {
            console.log('Route or destiny is unavailable.');
        }
        getBestPrice();

    } catch (err) {
        return console.log('err', err);
    }
};

getBestPrice();