const handleFile = require('./handleFile');

async function validate(data) {

    let isValid = true;
    let message = '';

    if (!(!!data.from)) {
        isValid = false;
        message = 'field from is required';

    } else if (!(!!data.to)) {
        isValid = false;
        message = 'field to is required';

    } else if (!(!!data.price)) {
        isValid = false;
        message = 'field price is required';

    } else if (!(!!parseInt(data.price))) {
        isValid = false;
        message = 'price must be a number';
    }

    return { isValid, message };
}

async function add(data) {

    const validation = await validate(data);

    if (validation.isValid) {
        await handleFile.add(data);
        validation.message = 'register has created';
    }

    return validation;
}

module.exports = { add };