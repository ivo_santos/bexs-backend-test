const csv = require('csv-parser');
const fs = require('fs');

const readFileData = async () => {
    return await new Promise((resolve, reject) => {

        const data = [];
        const file = __dirname + '/../db/input-file.csv';

        try {
            if (fs.existsSync(file)) {
                fs.createReadStream(file).pipe(csv()).on('data', (row) => {
                    data.push(row);
                }).on('end', () => {
                    resolve(data);
                });
            }
        } catch (error) {
            throw Error;
        }
    });
}

const append = (file, text) => {
    fs.appendFileSync(file, '\n' + text);
}

const create = (file, text) => {
    const writeStream = fs.createWriteStream(file)
    writeStream.write('From,To,Price\n');
    writeStream.write(text);
    writeStream.end();

    writeStream.on('finish', () => {
    }).on('error', (err) => {
        throw Error;
    });
}

const add = async (data) => {

    const file = __dirname + '/../db/input-file.csv';
    const csvData = [data.from, data.to, data.price];
    const text = csvData.join(',').toUpperCase();

    try {

        if (fs.existsSync(file)) {
            append(file, text);

        } else {
            create(file, text);
        }

    } catch (err) {
        console.error(err);
        throw Error;
    }

}

module.exports = { readFileData, add };