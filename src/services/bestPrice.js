const handleFile = require('./handleFile');

async function get(from, to) {

    const directRoutes = await groupDirectRoutes();
    return dijkstra(directRoutes, from, to);
}

async function groupDirectRoutes() {

    const directRoutes = {};
    const data = await handleFile.readFileData();

    data.forEach(row => {

        const keyExists = (!!directRoutes[row.From]);

        if (!keyExists) {
            directRoutes[row.From] = {};
        }

        const key = String(row.To);
        const price = parseInt(row.Price)
        directRoutes[row.From][key] = price;

    });

    return directRoutes;
}

function dijkstra(directRoutes, from, to) {

    if (!directRoutes[from] || from === to) {
        throw Error;
    }

    let result = null;

    const data = { [from]: { route: [], total: 0 } };

    while (true) {

        let defaultPrice = Infinity;
        let newRoutes = null;
        let routeLowestPrice = null;
        let keep = false;

        for (let vertex in data) {

            if (!data[vertex]) {
                continue;
            }

            const total = data[vertex].total;
            const adjacents = directRoutes[vertex];

            for (let adjacent in adjacents) {

                if (data[adjacent]) {
                    continue;
                }

                const newTotal = (adjacents[adjacent] + total);

                if (newTotal < defaultPrice) {

                    newRoutes = data[vertex].route;
                    routeLowestPrice = adjacent;
                    defaultPrice = newTotal;
                    keep = true;
                }
            }
        }

        if (!keep) {
            if (!!data[to]) {
                result = data[to];
                result.route = [from].concat(result.route);
            }

            break;
        }

        data[routeLowestPrice] = {
            route: newRoutes.concat(routeLowestPrice),
            total: defaultPrice
        };
    }

    return result;
}

module.exports = { get };