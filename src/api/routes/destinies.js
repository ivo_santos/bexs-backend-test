const express = require('express');
const router = express.Router();
const bestPrice = require('../../services/bestPrice');
const handleData = require('../../services/handleData');

router.get('/v1/destinies/:from/:to/best-price', async (req, res) => {

  const from = req.params.from.toUpperCase();
  const to = req.params.to.toUpperCase();

  try {

    const data = await bestPrice.get(from, to);

    if (!!data) {

      const response = { routes: data.route, price: data.total };
      res.json(response);

    } else {

      const response = { error: 001, message: 'Route or destiny is unavailable.' };
      res.status(404).json(response);
    }

  } catch (error) {

    const response = { error: 001, message: 'Route or destiny is unavailable.' };
    res.status(404).json(response);
  }

  res.end();
});

router.post('/v1/destinies', async (req, res) => {

  try {

    const data = {
      from: req.body.from,
      to: req.body.to,
      price: req.body.price
    };

    const added = await handleData.add(data);

    if (added.isValid) {
      const response = { message: added.message };
      res.status(201).json(response);

    } else {

      const response = { error: 003, message: added.message };
      res.status(400).json(response);
    }

  } catch (error) {

    const response = { error: 002, message: 'error when try to add destiny' };
    res.status(404).json(response);
  }

  res.end();
});

module.exports = router;