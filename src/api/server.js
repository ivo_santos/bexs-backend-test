const express = require('express');
const http = require('http');
const routes = require('./routes/destinies');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(routes);

const server = http.createServer(app);
server.listen(process.env.PORT || 3000);