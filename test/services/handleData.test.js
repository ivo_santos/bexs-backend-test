const { stub } = require('sinon');
const { expect } = require('chai');
const handleFile = require('../../src/services/handleFile');
const handleData = require('../../src/services/handleData');

describe('handleData service', async () => {

    it('it should return valid when valid request data was sended', async () => {

        stub(handleFile, 'add').returns();

        const data = {
            from: 'BRC',
            to: 'BSB',
            price: 22
        };
        const result = await handleData.add(data);

        expect(result).to.have.keys(['isValid', 'message']);
        expect(result.isValid).to.be.true;

        // it's a weak assertion
        // expect(result.message).to.equal('register has created');
    });

    it('it should return invalid when field required does not exist.', async () => {

        const data = {};
        const result = await handleData.add(data);

        expect(result).to.have.keys(['isValid', 'message']);
        expect(result.isValid).to.be.false;
    });

});