const { stub } = require('sinon');
const { expect } = require('chai');
const bestPrice = require('../../src/services/bestPrice')
const handleFile = require('../../src/services/handleFile');

describe('bestPrice service', async () => {

    const mockDirectRoutes = [
        { From: 'GRU', To: 'BRC', Price: '50' },
        { From: 'GRU', To: 'SCL', Price: '80' },
        { From: 'GRU', To: 'CDG', Price: '10' },
        { From: 'BRC', To: 'SCL', Price: '5' }
    ];

    before(() => {
        stub(handleFile, 'readFileData').returns(Promise.resolve(mockDirectRoutes));
    })

    it('it should get route: GRU-BRC-SCL, price: 55 when route from GRU to SCL.', async () => {

        const expected = {
            route: [
                "GRU",
                "BRC",
                "SCL",
            ],
            total: 55
        };

        const from = 'GRU';
        const to = 'SCL';
        const result = await bestPrice.get(from, to);

        expect(result).to.deep.equal(expected);
    });

    it('it should returns null when no existent route GRU-BSB', async () => {

        const from = 'GRU';
        const to = 'BSB';
        const result = await bestPrice.get(from, to);

        expect(result).to.be.null;
    });
});